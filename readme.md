Adapation du jeu "Au jardin" de la compilation d'applications Clicmenu.

L'activité consiste à constituer des collections d'objets selon une quantité donnée, de 1 à 10 ou de 10 à 20.

L'application peut être testée en ligne [ici](https://primtux.fr/applications/aujardin/index.html)